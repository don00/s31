// 5. Import the http module using the required directive.
const http = require("http");

// 6. Create a variable port and assign it with the value of 3000.
const port = 3000;

// 7. Create a server using the createServer method that will listen in to the port provided above.
const server = http.createServer((request, response) => {

	// 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	if (request.url == '/login'){
		response.writeHead(200, {'Content-type' : 'text/plain'});
		response.end('You are in the login page');
	
	// 11. Create a condition for any other routes that will return an error message.
	} else { 
		response.writeHead(404, {'Content-type' : 'text/plain'});
		response.end('Page not found! Please check the address and try again.');
	};
});

server.listen(port);

// 8. Console log in the terminal a message when the server is successfully running.
console.log(`Server is running at localhost: ${port}`);

